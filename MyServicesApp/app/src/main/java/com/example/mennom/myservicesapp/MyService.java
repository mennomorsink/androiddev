package com.example.mennom.myservicesapp;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.util.Log;

import java.io.IOException;

public class MyService extends Service {

    private MediaPlayer mPlayer;

    public MyService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Log.d("PIPOO", "onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.d("PIPOO", "onStartCommand");

        String command = intent.getStringExtra("COMMAND");

        if(command.equals("START"))
        {
            startPlay();
        }
        if(command.equals("PAUSE"))
        {
            pausePlay();
        }
        if(command.equals("STOP"))
        {
            stopPlay();
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Log.d("PIPOO", "onDestroy");
        stopPlay();

        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return null;
    }

    private void startPlay() {
        if(mPlayer == null)
        {
                mPlayer = MediaPlayer.create(getApplicationContext(), R.raw.elevator_music);
                //mPlayer.prepare();
                mPlayer.setLooping(true);
                mPlayer.start();

        }
        else if (mPlayer != null)
        {
            mPlayer.start();
        }
    }

    private void pausePlay() {
        if(mPlayer != null && mPlayer.isPlaying())
        {
            mPlayer.pause();
        }
    }

    private void stopPlay()
    {
        if(mPlayer != null)
        {
            mPlayer.stop();
            mPlayer.reset();
            mPlayer.release();
        }

        mPlayer = null;
    }
}
