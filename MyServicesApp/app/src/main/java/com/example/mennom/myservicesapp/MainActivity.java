package com.example.mennom.myservicesapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


public class MainActivity extends Activity {

    boolean wasPlaying = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    TelephonyManager telephony;
    public void startService(View v) {
        // use this to start and trigger a service
        Intent i= new Intent(this, MyService.class);
        // potentially add data to the intent
        i.putExtra("COMMAND", "START");
        this.startService(i);
        wasPlaying = true;

        MyPhoneStateListener phoneListener = new MyPhoneStateListener(this);
        telephony = (TelephonyManager) getApplicationContext()
                .getSystemService(Context.TELEPHONY_SERVICE);
        telephony.listen(phoneListener, PhoneStateListener.LISTEN_CALL_STATE);
    }

    public void pauseService() {
        // use this to start and trigger a service
        Intent i= new Intent(this, MyService.class);
        // potentially add data to the intent
        i.putExtra("COMMAND", "PAUSE");
        this.startService(i);
    }

    public void stopService(View v) {
        // use this to start and trigger a service
        Intent i= new Intent(this, MyService.class);
        // potentially add data to the intent
        i.putExtra("COMMAND", "STOP");
        this.stopService(i);

        wasPlaying = false;
    }

    public class MyPhoneStateListener extends PhoneStateListener {

        public Boolean phoneRinging = false;
        private MainActivity _mainActivity;

        public MyPhoneStateListener(MainActivity mainActivity)
        {
            _mainActivity = mainActivity;
        }

        public void onCallStateChanged(int state, String incomingNumber) {

            switch (state) {
                case TelephonyManager.CALL_STATE_IDLE:
                    Log.d("DEBUG", "IDLE");
                    phoneRinging = false;
                    if(wasPlaying) {
                        _mainActivity.startService((View) null);
                    }
                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK:
                    Log.d("DEBUG", "OFFHOOK");
                    phoneRinging = false;
                    break;
                case TelephonyManager.CALL_STATE_RINGING:
                    Log.d("DEBUG", "RINGING");
                    phoneRinging = true;
                    _mainActivity.pauseService();
                    break;
            }
        }

    }
}

