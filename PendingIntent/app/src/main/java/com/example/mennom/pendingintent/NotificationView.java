package com.example.mennom.pendingintent;

import android.app.Activity;
import android.app.NotificationManager;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by MennoM on 20-11-2014.
 */
public class NotificationView extends Activity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification);

        NotificationManager manager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        int id = getIntent().getExtras().getInt("NotificationID");
        manager.cancel(id);

        TextView tv = (TextView)findViewById(R.id.textView);
        tv.setText("Notification ID: " + id);


    }
}
